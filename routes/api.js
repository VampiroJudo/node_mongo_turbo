// Full Documentation - https://docs.turbo360.co
const express = require('express')
const router = express.Router()

const Profile = require('../models/Profile')

router.get('/profile',  (req, res) => {
	const query = req.query

	let filters = req.query
	if (req.query.age != null) {
		filters = {
			age: {$gt: req.query.age}
		}
	}

	Profile.find(filters)
	.then(profiles => {
		res.json({
			confirmation: 'success',
			data: profiles
		})
	})
	.catch(err => {
		res.json({
			confimation: 'fail',
			message: err.message
		})
	})
})


// Non-RESTful
router.get('/profile/update', (req, res) => {
	const query = req.query
	const profileId = query.id
	delete query['id']

	Profile.findByIdAndUpdate(profileId, query, {new:true})
	.then(profile => {
		res.json({
			confirmation: 'success',
			data: profile
		})
	})
	.catch(err => {
		res.json({
			confirmation: 'fail',
			message: err.message
		})
	})
})

router.get('/profile/remove', (req, res) => {
	const query = req.query

	Profile.findByIdAndRemove(query.id)
	.then(data => {
		res.json({
			confimation: 'sucess',
			data: 'Profile ' +query.id+ ' successfully removed.'
		})
	})
	.catch(err => {
		res.json({
			confirmation: 'fail',
			message: err.message
		})
	})
})

router.get('/profile/:id',  (req, res) => {
	const id = req.params.id

	Profile.findById(id)
	.then(profile => {
		res.json({
			confirmation: 'success',
			data: profile
		})
	})
	.catch(err => {
		res.json({
			confimation: 'fail',
			message: 'Profile ' + id + ' not found.'
		})
	})
})

router.post('/profile', (req, res) => {

	Profile.create(req.body)
	.then(profile => {
		res.json({
			confirmation: 'success',
			data: profile
		})
	})
	.catch(err => {
		res.json({
			confimation: 'fail',
			message: err.message
		})
	})
})


module.exports = router